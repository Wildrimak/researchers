# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from ifpi_students.models import IfpiStudent

def index(request):
	return render(request, 'see_ifpi_students.html', {'ifpiStudents' : IfpiStudent.objects.all()})

def add(request):
	return render(request, 'form_ifpi_student.html')
