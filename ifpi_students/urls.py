from django.conf.urls import url
from ifpi_students import views

urlpatterns = [
	url(r'^ifpi_students$', views.index, name='SeeIfpiStudents'),
	url(r'^ifpi_students/new$', views.add, name='AddIfpiStudent')
]