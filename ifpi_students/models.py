# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from members.models import Member

class IfpiStudent(models.Model):
	enroll = models.CharField(primary_key=True, max_length=12)
	is_scholarship_holder = models.BooleanField()
	member = models.ForeignKey(Member)

	def __str__(self):
		return self.member.name + " " + self.enroll