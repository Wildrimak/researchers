from django.conf.urls import url
from acting_members import views

urlpatterns = [
	url(r'^acting_members$', views.index, name='SeeActingMembers'),
	url(r'^acting_members/new$', views.add, name='AddActingMember')
]