# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from members.models import Member
from performances.models import Performance

# Create your models here.

class ActingMember(models.Model):
	member = models.ForeignKey(Member)
	performance = models.ForeignKey(Performance)

	def __str__(self):
		return self.member.name + " " + self.performance.name