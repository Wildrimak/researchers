# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from acting_members.models import ActingMember
admin.site.register(ActingMember)