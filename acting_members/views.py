# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from acting_members.models import ActingMember

def index(request):
	return render(request, 'see_acting_members.html', {'actingMembers' : ActingMember.objects.all()})

def add(request):
	return render(request, 'form_acting_member.html')
