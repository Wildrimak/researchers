from django.conf.urls import url
from institutions import views

urlpatterns = [
	url(r'^institutions$', views.index, name='SeeInstitutions'),
	url(r'^institutions/new$', views.add, name='AddInstitution')
]