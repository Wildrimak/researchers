# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from institutions.models import Institution

def index(request):
	return render(request, 'see_institutions.html', {'institutions' : Institution.objects.all()})

def add(request):
	return render(request, 'form_institution.html')
