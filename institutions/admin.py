# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin

from institutions.models import Institution

admin.site.register(Institution)