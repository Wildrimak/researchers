# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from specialties.models import Specialty
admin.site.register(Specialty)