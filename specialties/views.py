# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from specialties.models import Specialty

def index(request):
	return render(request, 'see_specialties.html', {'specialties' : Specialty.objects.all()})

def add(request):
	return render(request, 'form_specialty.html')