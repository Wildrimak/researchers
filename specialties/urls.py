from django.conf.urls import url
from specialties import views

urlpatterns = [
	url(r'^specialties$', views.index, name='SeeSpecialties'),
	url(r'^specialties/new$', views.add, name='AddSpecialty')
]