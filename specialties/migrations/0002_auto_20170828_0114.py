# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-08-28 01:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('specialties', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='specialty',
            name='description',
            field=models.CharField(max_length=45000),
        ),
    ]
