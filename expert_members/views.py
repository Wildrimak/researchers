# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from expert_members.models import SpecialtyMember

def index(request):
	return render(request, 'see_expert_members.html', {'expertMembers' : SpecialtyMember.objects.all()})

def add(request):
	return render(request, 'form_expert_member.html')
