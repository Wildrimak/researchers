# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from members.models import Member
from specialties.models import Specialty

class SpecialtyMember(models.Model):
	member = models.ForeignKey(Member)
	specialty = models.ForeignKey(Specialty)

	def __str__(self):
		return self.member.name + " " + self.specialty.name