# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from expert_members.models import SpecialtyMember
admin.site.register(SpecialtyMember)

# Register your models here.
