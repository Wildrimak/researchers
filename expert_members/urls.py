from django.conf.urls import url
from expert_members import views

urlpatterns = [
	url(r'^expert_members$', views.index, name='SeeExpertMembers'),
	url(r'^expert_members/new$', views.add, name='AddExpertMember')
]