# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from performances.models import Performance

def index(request):
	return render(request, 'see_performances.html', {'performances' : Performance.objects.all()})

def add(request):
	return render(request, 'form_performance.html')
