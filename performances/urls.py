from django.conf.urls import url
from performances import views

urlpatterns = [
	url(r'^performances$', views.index, name='SeePerformances'),
	url(r'^performances/new$', views.add, name='AddPerformance')
]