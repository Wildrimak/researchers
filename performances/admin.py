# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin

from performances.models import Performance
admin.site.register(Performance)