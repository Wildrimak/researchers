from django.conf.urls import url
from positions import views

urlpatterns = [
	url(r'^positions$', views.index, name='SeePositions'),
	url(r'^positions/new$', views.add, name='AddPosition')
]