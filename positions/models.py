# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Position(models.Model):
	name = models.CharField(max_length=500)
	description = models.CharField(max_length=45000)

	def __str__(self):
		return self.name
