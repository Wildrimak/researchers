from django import forms
from django.contrib.auth.models import User

class RegistrarCargoForm(forms.Form):
	identify = forms.IntegerField(required=True)
	nome = forms.CharField(required=True)
	descricao = forms.CharField(required=True)

	def is_valid(self):
		valid = True
		if not super(RegistrarCargoForm, self).is_valid():
			self.adiciona_erro('Por favor, verifique os dados informados')
			valid = False

		return valid

	def adiciona_erro(self, message):
		errors = self._errors.setdefault(forms.forms.NON_FIELD_ERRORS, forms.utils.ErrorList())
		errors.append(message)


