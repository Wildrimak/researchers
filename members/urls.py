from django.conf.urls import url
from members import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^members$', views.see, name='SeeMembers'),
    url(r'^members/new$', views.add, name='AddMember'),
]