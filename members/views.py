# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from members.models import Member

def index(request):
	return render(request, 'index.html')

def see(request):
	return render(request, 'see_members.html', {'members' : Member.objects.all()})

def add(request):
	return render(request, 'form_member.html')