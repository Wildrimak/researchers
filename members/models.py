# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from positions.models import Position
from institutions.models import Institution

class Member(models.Model):
	name = models.CharField(max_length=500)
	cpf = models.CharField(max_length=20)
	rg = models.CharField(max_length=20)
	contact = models.CharField(max_length=20)
	is_active = models.BooleanField()
	date_of_entry = models.DateField(blank=True, null=True)
	shutdown_date = models.DateField(blank=True, null=True)
	position = models.ForeignKey(Position, models.DO_NOTHING)
	institution = models.ForeignKey(Institution, models.DO_NOTHING)

	def __str__(self):
		return self.name